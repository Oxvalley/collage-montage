package oxvalley.core.image.ui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextArea;

import common.ui.components.ext.CompExtEnum;
import common.ui.components.ext.JPanelExt;

public class CollageMontageUI extends JFrame
{
   private static final long serialVersionUID = 1L;

   private static final String PICTURE_FOLDER_PATH = null;

   // TODO Change to name of your icon
   private String ICON_NAME = null;

   // TODO init fields here


   /////////// Java GUI Designer version 1.1 Starts here. ////
   // Text below will be regenerated automatically.
   // Generated 2018-05-05 10:54:53

   private JPanelExt iPanelExt;
   private Palette iPalette1;
   private Palette txt2;
   private JTextArea txt3;
   private JButton btnGenerate;
   private JTextArea txt6;

   private int width = 769;
   private int height = 637;

   public static void main(String[] args)
   {
      CollageMontageUI collageMontageUI = new CollageMontageUI();
      collageMontageUI.setVisible(true);
   }

   public CollageMontageUI()
   {
      super();
      setTitle("Collage Montage v 1.0");
      setSize(width, height);
      if (ICON_NAME != null)
      {
         setIconImage(getToolkit().getImage(ICON_NAME));
      }

      getContentPane().setLayout(new BorderLayout());
      iPanelExt = new JPanelExt(width, height);
      iPanelExt.setLayout(null);
      getContentPane().add(iPanelExt, BorderLayout.CENTER);

      iPalette1 = new Palette();
      iPanelExt.addComponentExt(iPalette1, 3, 29, 404, 324, CompExtEnum.ResizeWithWidthAndHeight);

      txt2 = new Palette();
      iPanelExt.addComponentExt(txt2, 471, 29, 231, 377, CompExtEnum.ResizeWithWidthAndHeight);

      btnGenerate = new JButton("Generate");
      iPanelExt.addComponentExt(btnGenerate, 295, 431, 119, 48, CompExtEnum.MoveWithHeight);
      btnGenerate.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent e)
         {
            btnGenerateClicked(e);
         }
      });

      txt6 = new JTextArea();
      iPanelExt.addComponentExt(txt6, 19, 504, 746, 75, CompExtEnum.MoveWithHeight);

      setDefaultCloseOperation(EXIT_ON_CLOSE);

      /////////// Java GUI Designer version 1.1 Generation ends here. ///

      // TODO Add init declarations here

   }

   private void btnGenerateClicked(ActionEvent e)
   {
      MontageWorker mw = new MontageWorker(1000,600, iPalette1);
      mw.plotImages(PICTURE_FOLDER_PATH);

   }

}
