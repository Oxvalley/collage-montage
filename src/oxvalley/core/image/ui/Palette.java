package oxvalley.core.image.ui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Toolkit;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JLayeredPane;

import common.utils.LogUtil;

public class Palette extends JLayeredPane
{
   private int iWidth;
   private int iHeight;
   private String iImageDir = "Z:/photo/Skolfoton/";
   private Map<String, Image> iImageCache = new HashMap<String, Image>();
   private Toolkit iToolkit = Toolkit.getDefaultToolkit();

   @Override
   protected void paintComponent(Graphics g)
   {
      iWidth = getWidth();
      iHeight = getHeight();

      g.setColor(Color.BLACK);
      g.drawRect(0, 0, iWidth - 1, iHeight - 1);

      g.setColor(Color.BLUE);
      drawRect(g, 5.0, 5.0, 60.0, 70.0);
      drawImage(g, "A2005.jpg", 7.0, 7.0, 56.0, 66.0);
   }

   private void drawImage(Graphics g, String fileName, double x, double y,
         double width, double height)
   {
      int xPixels = scaleX(x);
      int yPixels = scaleY(y);
      int widthPixels = scaleX(width);
      int heightPixels = scaleY(height);
      Image image = getImage(fileName);
      g.drawImage(image, xPixels, yPixels, widthPixels, heightPixels, null);
   }

   private void drawRect(Graphics g, double x, double y, double width,
         double height)
   {
      int xPixels = scaleX(x);
      int yPixels = scaleY(y);
      int widthPixels = scaleX(width);
      int heightPixels = scaleY(height);
      g.drawRect(xPixels, yPixels, widthPixels, heightPixels);
   }

   private int scaleX(double x)
   {
      return (int) Math.round(iWidth * x / 100.0);
   }

   private int scaleY(double x)
   {
      return (int) Math.round(iHeight * x / 100.0);
   }

   public Image getImage(String fileName)
   {
      String filePath = iImageDir + "/" + fileName;

      Image image = iImageCache.get(filePath);

      if (image == null)
      {
         File file = new File(filePath);
         if (file.exists())
         {
            image = iToolkit.getImage(filePath);
            MediaTracker mediaTracker = new MediaTracker(this);
            mediaTracker.addImage(image, 1);
            try
            {
               mediaTracker.waitForID(1);
            }
            catch (InterruptedException ie)
            {
               LogUtil.printDialog("Could not load image " + filePath);
               return null;
            }
            if (image.getWidth(null) == -1)
            {
               LogUtil.printDialog(
                     "Could not load image " + filePath + " . Width=-1");
               return null;

            }

            iImageCache.put(filePath, image);

         }
         else
         {
            System.out.println(
                  "File " + file.getAbsolutePath() + " does not exist!");
            return null;
         }

      }
      return image;
   }
}
